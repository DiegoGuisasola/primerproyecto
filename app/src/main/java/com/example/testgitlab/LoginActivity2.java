package com.example.testgitlab;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

public class LoginActivity2 extends AppCompatActivity {

    ImageView ivLogo;
    TextInputLayout tilEmail;
    TextInputEditText etEmail;
    TextInputLayout tilPassword;
    TextInputEditText etPassword;

    AppCompatButton btnLogin;
    AppCompatButton btnFacebook;
    AppCompatButton btnGoogle;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login2);

        initUI();
    }

    private void initUI() {
        ivLogo = findViewById(R.id.iv_logo);

        etEmail = findViewById(R.id.et_email);
        etEmail.setText("tuDireccion@correo.com");

        etPassword = findViewById(R.id.et_password);
        etPassword.setText("Abcd1234");

        btnLogin= findViewById(R.id.btn_login);
        btnLogin.setOnClickListener((evt) -> { OnLoginClick(); });

        btnFacebook = findViewById(R.id.btn_facebook);
        btnFacebook.setOnClickListener((evt) -> { OnFacebookClick(); });

        btnGoogle = findViewById(R.id.btn_google);
        btnGoogle.setOnClickListener((evt) -> { OnGoogleClick(); });

    }

    private void OnLoginClick(){
        etEmail.setText("");
        etPassword.setText("");
        Toast.makeText(this, "Inicio de Sesión exitoso.", Toast.LENGTH_SHORT).show();

        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    private void OnFacebookClick(){

    }

    private void OnGoogleClick(){

    }
}